<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AUTO KADA</title>

        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="inv_header_container">
            <div class="inv_main_content">
                <img class="logo" src="css/images/logo-trans.png" width="354" height="67" alt="AUTOKADA">
                <div class="inv_main_headline_holder"><h1>AUTO KADA Inventerezācija</h1></div>
                <div class="inv_nav_tabs_holder">
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="inv_btn active" id="stats"><a href="#">Statistika</a></li>
                        <li role="presentation" class="inv_btn" id="export"><a href="#">Exportēt</a></li>
                        <li role="presentation" class="inv_btn" id="uploadBins"><a href="#">Augšuplādēt plauktus</a></li>
                        <li role="presentation" class="inv_btn" id="uploadItems"><a href="#">Augšuplādēt Preču kodus</a></li>
                        <li role="presentation" class="inv_btn" id="sysFunction"><a href="#">Sistēmas Funkcijas</a></li>
                        <li role="presentation" class="inv_btn" id="withoutMovementView"><a href="#">Bez kustības apskats</a></li>
                        <li role="presentation" class="inv_btn" id="withoutMovement"><a href="#">Bez kustības atjaunināšana</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="inv_middle_container">
            <div class="inv_main_content inv_top_shadow inv_middle_container_style">
                <div class="container-fluid">

                </div>
            </div>

        </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/Controllers/uplodabins.js"></script>
    <script src="js/Controllers/statistics.js"></script>
    <script src="js/Controllers/uploaditems.js"></script>
    <script src="js/Controllers/sysfunctions.js"></script>
    <script src="js/Controllers/withoutmovement.js"></script>
    <script src="js/Controllers/withoutmovementview.js"></script>
    <script src="js/scripts.js"></script>
    <script type="text/javascript">
        //alert("dd");
        InventoryApp.initialize();

    </script>
    </body>
</html>