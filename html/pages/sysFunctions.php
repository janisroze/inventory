
<h3>Sistēmas funkcijas</h3>
<button  class="btn btn-default" style="float: left" id="sys-btn-update-bins">
    Atjaunot plauktus no Navīzijas!
</button>
<button  class="btn btn-default" style="float: left" id="sys-btn-update-items">
    Atjaunot prece no Navīzijas!
</button>
<button  class="btn btn-default" style="float: left" id="sys-btn-move-to-backup">
    Pārvietot aktuālo inventerizācijas informāciju uz rezervi!
</button>
<div style="border: none; margin-top: 20px;" id="resultDiv">
</div>
<div style="border: none; margin-top: 20px;" id="resultDiv1">
</div>
<script type="text/javascript">
    sysFunctionsController.initialize();
</script>