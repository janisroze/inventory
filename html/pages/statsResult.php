<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15.26.8
 * Time: 17:15
 */
$location = $_GET['location'];
$DBServer="localhost";
$DBUser="root";
$DBPass="root";
$DBName="inventory";
$conn2 = new mysqli($DBServer, $DBUser, $DBPass, $DBName);
// check connection
if ($conn->connect_error) {
     trigger_error('Database connection failed: ' . $conn->connect_error, E_USER_ERROR);
}
$query="select @ir := (select count(distinct b.bin_id) from inventory as b, StockingIds as a where b.bin_id=a.index and a.location='" . $location . "') as 'ir',
        @kopa :=(select count(a.bin) from StockingIds as a where a.location='" . $location . "') as 'kopa',
         round(@ir/@kopa*100, 2) as proc, (@kopa-@ir) as nav,
          (select count(distinct a.barcode) from inventory as a, StockingIds as b where a.bin_id=b.index and b.location='" . $location . "') as disbar,
           (select count(a.barcode) from inventory as a, StockingIds as b where a.bin_id=b.index and b.location='" . $location . "') as scnbar,
            (select sum(a.count) from inventory as a, StockingIds as b where a.bin_id=b.index and b.location='" . $location . "') as allitems";
if ($result = $conn2->query($query)) {
    while($row = $result->fetch_array()) {
        echo '<div class="span12">
      <div class="progress progress-bar-success progress-striped active">
        <div class="bar" style="width: ' . $row["proc"] . '%">' . $row["proc"] . '%</div>
      </div>
    </div>
  </div>';
        echo '<div>
                <div class="inv_stats_info">Noskanēti plaukti:<span class="label label-success">' . $row["ir"] . '</span></div>
                <div class="inv_stats_info">Kopā plaukti:<span class="label label-default">' . $row["kopa"] . '</span></div>
                <div class="inv_stats_info">Nenoskanēti plaukti:<span class="label label-important">' . $row["nav"] . '</span></div>
                <div class="inv_stats_info">Unikālie noskanētie svītru kodi:<span class="label label-info">' . $row["disbar"] . '</span></div>
                <div class="inv_stats_info">Noskanētie svītru kodi:<span class="label label-info">' . $row["scnbar"] . '</span></div>
                <div class="inv_stats_info">Kopējais saskaitīto preču skaits:<span class="label label-info">' . $row["allitems"] . '</span></div>
            </div>';
    }

}


$query="SELECT a.bin as 'plaukts', (select count(*) from inventory as b where b.bin_id=a.index) as 'summa', (select sum(b.count) from inventory as b where b.bin_id=a.index) as 'summa2' FROM StockingIds as a WHERE a.location='" . $location . "' ORDER BY summa ASC";
//echo $query;
if ($result = $conn2->query($query)) {
    echo '<div class="stats_table">';
    echo '<table class="table table-striped table-bordered">';
    echo '<tr><th>Plaukts</th><th>Skanējumi uz plaukta</th><th>Kopējais produktu skaits uz plaukta</th><th>Status</th></tr>';
    while($row = $result->fetch_array()) {
        echo '<tr><td>' . $row["plaukts"] . '</td><td>' . $row["summa"] . '</td><td>' . $row["summa2"] . '</td>';
        if($row["summa"]>0){
            echo '<td><span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="color: #04bb00"></span></td>';
        } else{
            echo '<td><span class="glyphicon glyphicon-remove-sign" aria-hidden="true" style="color: #bb000c"></span></td>';
        }
        echo '</tr>';
   }
    echo '</table>';
    echo '</div>';
}
$result->close();
