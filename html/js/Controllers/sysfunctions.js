/**
 * Created by root on 15.8.9.
 */

var sysFunctionsController = {
    eventsBinded : false,
    done : false,
    interval : null,
    // Application Constructor
    initialize : function() {
        window.console.log("in initialize");

        sysFunctionsController.bindEvents();
    },
    bindEvents : function() {
        if (!this.eventsBinded) {

        $(document).on("click", "#sys-btn-update-bins", sysFunctionsController.updateBins);
            $(document).on("click", "#sys-btn-update-items", sysFunctionsController.updateItems);
            $(document).on("click", "#sys-btn-move-to-backup", sysFunctionsController.moveToBackup);

        this.eventsBinded=true;
    }
    },
    updateBins : function() {
        sysFunctionsController.done=false;
        $("#resultDiv1").load('pages/updateBins.php', function(){
            sysFunctionsController.done=true;
        });
        sysFunctionsController.timer(130);
    },
    updateItems : function() {
        sysFunctionsController.done=false;
        $("#resultDiv1").load('pages/updateItems.php', function(){
            sysFunctionsController.done=true;
        });

        sysFunctionsController.timer(300);
    },
    moveToBackup : function() {
        sysFunctionsController.done=false;
        $("#resultDiv1").load('pages/moveToBackup.php', function(){
            sysFunctionsController.done=true;
        });

        sysFunctionsController.timer(10);
    },
    timer : function(duration){
        var timer = duration, minutes, seconds;
        var ttt =function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            if(sysFunctionsController.done){
                clearInterval(sysFunctionsController.interval);
                $("#resultDiv").text("Darbs pabeigts");
            } else {
                $("#resultDiv").text("Atlikušais aptuvenais laiks: "+minutes + ":" + seconds);

                if (--timer < 0) {
                    timer = duration;
                }
            }


        };
        sysFunctionsController.interval=setInterval(ttt, 1000);


        ///$('#bins_form').submit();
    }




};