
var statisticsController = {
    intervalHolder : null,
    location : null,
    eventsBinded : false,
    // Application Constructor
    initialize : function() {
        window.console.log("in initialize");

        statisticsController.bindEvents();
    },
    bindEvents : function() {
        if (!this.eventsBinded) {
            window.console.log("in binder");
            $(document).on("click", "#stats_btn", statisticsController.load);
            this.eventsBinded=true;
        }

    },
    load : function() {


        statisticsController.location=$("#statistics_select").val();
        $("#stats_container").load("pages/statsResult.php?location="+statisticsController.location);
        statisticsController.intervalHolder=setInterval(function(){
            $("#stats_container").load("pages/statsResult.php?location="+statisticsController.location);
        }, 180*1000);
    },
    submit : function(){


        ///$('#bins_form').submit();
    }




};