var InventoryApp = {

    // Application Constructor
    initialize : function() {
        window.console.log("in initialize");

        InventoryApp.bindEvents();
    },
    bindEvents : function() {
        window.console.log("in bind events");
        $(document).on("click", ".inv_btn", function(event) {
            $(".nav-tabs .active").removeClass("active");
            $(this).addClass('active');
            InventoryApp.navigate($(this).attr("id"));


        });
        InventoryApp.navigate('stats');
    },
    onDeviceReady : function() {
        window.console.log("dev ready");
    },
    navigate : function(id) {
        if(id=='export'){
            $(".container-fluid").load("pages/export.php");
        } else if(id=='uploadBins'){
            $(".container-fluid").load("pages/uploadBinsStart.php");
        } else if(id=='stats'){
            $(".container-fluid").load("pages/statistics.php");
        } else if(id=='uploadItems'){
            $(".container-fluid").load("pages/uploadItemsStart.php");
        } else if(id=='sysFunction'){
            $(".container-fluid").load("pages/sysFunctions.php");
        } else if(id=='withoutMovement'){
            $(".container-fluid").load("pages/updateWithoutMovementStart.php");
        } else if(id=='withoutMovementView'){
            $(".container-fluid").load("pages/withoutMovementView.php");
        }

    },
    getStats : function() {
        //alert("events");

    }




};
